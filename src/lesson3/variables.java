package lesson3;

public class variables {
    public static void main(String[] args) {
        byte myByte = 0;

        System.out.println(myByte);

        short myShort = 123;

        System.out.println(myShort);

        int myInt = 123456;

        System.out.println(myInt);

        long myLong = 987654321;

        System.out.print(myLong);

        float myFloat = 31.44f;

        System.out.println(myFloat);

        double myDouble = 3.14159;

        System.out.println(myDouble);

        char myChar = 1067;

        System.out.println(myChar);

        boolean myBoolean = false;

        System.out.println(myBoolean);

        String myString = "This is my code!";

        System.out.println(myString);
    }
}