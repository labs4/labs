package lesson5;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class task1 {
    public static void main(String[] args) throws IOException {
        BufferedReader bis = new BufferedReader(new InputStreamReader(System.in));
        int a = Integer.parseInt(bis.readLine());

        if(a==0){
            System.out.println("Зима");
        }
        else if(a==1){
            System.out.println("Весна");
        }
        else if(a==2){
            System.out.println("Лето");
        }
        else if(a==3){
            System.out.println("Осень");
        }
    }
}
