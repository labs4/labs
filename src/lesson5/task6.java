package lesson5;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class task6 {
    public static void main(String[] args) throws IOException {
        BufferedReader bis = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(bis.readLine());

        if((n > 0) && ((n & (n - 1)) == 0))
            System.out.println("Да");
        else
            System.out.println("Нет");

    }
}

