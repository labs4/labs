package lesson5;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class task5 {
    public static void main(String[] args) throws IOException {
        BufferedReader bis = new BufferedReader(new InputStreamReader(System.in));
        long n = Long.parseLong(bis.readLine());
        int countOfNumbers = 0;
        for ( ; n != 0 ; n /= 10)
            ++countOfNumbers;
        System.out.println(countOfNumbers);

    }
}

